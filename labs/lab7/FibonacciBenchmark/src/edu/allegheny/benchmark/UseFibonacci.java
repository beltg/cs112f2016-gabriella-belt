package edu.allegheny.benchmark;

import com.clarkware.Profiler;
import java.io.*;

public class UseFibonacci {

    public static void main(String[] args) {

        System.out.println("Begin experiment with different Fibonacci " +
                "implementations ..."); System.out.println();

	int ExperimentNum; //variables of the for loop (this is me) 
	int EndNum = 5;

        // extract the value that was passed on the command line; this is the
        // nth fibonacci number that we must calculate in the three different
        // fashions
        Integer Num = new Integer(args[0]); int num = Num.intValue();

        // determine which algorithm we are supposed to benchmark
        String chosen = args[1];
	
	//adding a third command-line argument to determine which algorithm we are supposed to benchmark (this is me)
	String chosen2 = args[2]; 


        if( chosen.equals("recursive") || chosen.equals("all") ) {

            // 1. RECURSIVE fibonacci (int)
            Profiler.begin("RecursiveFibonacciInt"); int recursiveFib =
                RecursiveFibonacci.fib(num);
            Profiler.end("RecursiveFibonacciInt");

            System.out.println("(Recursive/int) The " + num + "th Fibonacci " +
                    "number = " + recursiveFib + ".");

            // 1. RECURSIVE fibonacci (long)
            Profiler.begin("RecursiveFibonacciLong"); long recursiveFibLong =
                RecursiveFibonacci.fibLong(num);
            Profiler.end("RecursiveFibonacciLong");

            System.out.println("(Recursive/long) The " + num + "th Fibonacci "
                    + "number = " + recursiveFibLong + ".");

        }
	//this is me
	if( chosen2.equals("recursive") || chosen.equals("long") ) {
	
	// 1. RECURSIVE fibonacci (long) //still me 
            Profiler.begin("RecursiveFibonacciLong"); long recursiveFibLong =
                RecursiveFibonacci.fibLong(num);
            Profiler.end("RecursiveFibonacciLong");

            System.out.println("(Recursive/long) The " + num + "th Fibonacci "
                    + "number = " + recursiveFibLong + ".");

        }
        if( chosen.equals("iterative") || chosen.equals("all") ) {


            // 2. ITERATIVE fibonacci (int)
            Profiler.begin("IterativeFibonacciInt"); int iterativeFib =
                IterativeFibonacci.fib(num);
            Profiler.end("IterativeFibonacciInt");

            System.out.println("(Iterative/int) The " + num + "th Fibonacci " +
                    "number = " + iterativeFib + ".");

            // 2. ITERATIVE fibonacci (long)
            Profiler.begin("IterativeFibonacciLong"); long iterativeFibLong =
                IterativeFibonacci.fibLong(num);
            Profiler.end("IterativeFibonacciLong");

            System.out.println("(Iterative/long) The " + num + "th Fibonacci "
                    + "number = " + iterativeFibLong + ".");

        }
	//this is me
	if( chosen2.equals("iterative") || chosen.equals("long") ) {
	
	// 2. ITERATIVE fibonacci (long) //still me 
            Profiler.begin("IterativeFibonacciLong"); long iterativeFibLong =
                IterativeFibonacci.fibLong(num);
            Profiler.end("IterativeFibonacciLong");

            System.out.println("(Iterative/long) The " + num + "th Fibonacci "
                    + "number = " + iterativeFibLong + ".");
	}
	for (ExperimentNum = 0; ExperimentNum < EndNum; ExperimentNum++) {  //for loop so the experiment will run 5 times
	 
	  System.out.println(".");
	}

        System.out.println(); Profiler.print(new PrintWriter(System.out));

        System.out.println("... End experiment with different Fibonacci " +
                "implementations");

    }

}
