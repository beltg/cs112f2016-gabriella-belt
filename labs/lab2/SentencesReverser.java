import java.io.IOException;
import java.util.Scanner;
import java.util.Iterator;

public class SentencesReverser {

  public static void main(String[] args) throws IOException {
    Scanner scanner = new Scanner(System.in); //activating scanner up in here
    Sentences sentences = new Sentences(); //makes new instance of sentences.java
//the Sentences default constructor is called

    long beforeRead = System.currentTimeMillis();
    sentences.readSentencesFromFile();
    long afterRead = System.currentTimeMillis();
    System.out.println("Read time: " + (afterRead - beforeRead));

    beforeRead = System.currentTimeMillis();
    sentences.printSentences();
    afterRead = System.currentTimeMillis();
    System.out.println("Read time: " + (afterRead - beforeRead));

    beforeRead = System.currentTimeMillis();
    sentences.reverseSentences();
    afterRead = System.currentTimeMillis();
    System.out.println("Read time: " + (afterRead - beforeRead));

    beforeRead = System.currentTimeMillis();
    sentences.printSentences();
    afterRead = System.currentTimeMillis();
    System.out.println("Read time: " + (afterRead - beforeRead));
  }

}
