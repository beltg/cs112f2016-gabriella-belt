import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class Sentences {

  private Sentence[] sentences; //array is declared
  private int currentSentence; //variable is created/named
  private static final String SENTENCESFILE = "sentences.txt"; //named in memory, doesn't read txt
  private static final int NUMBER_OF_SENTENCES = 10; //int who's value is ten

  public Sentences() { //baby got null
    sentences = new Sentence[NUMBER_OF_SENTENCES]; //new array, with boundry of 10
    currentSentence = 0;    //set to 0, slot of array we're accessing -- unnessary due to defult
  }

  public void addSentence(Sentence sentence) {
    sentences[currentSentence] = sentence; //currentSentence and sentence are equal
    currentSentence = (currentSentence + 1) % NUMBER_OF_SENTENCES; //currentSentence witch is set to zero adds one
    //then the remainder is found,
    //the remainder currently equals 1 because i'm dividing by 10
  }

  public void readSentencesFromFile() throws IOException {
    Scanner fileScanner = new Scanner(new File(SENTENCESFILE)); //opened text file SENTENCESFILE
    while(fileScanner.hasNext()) { //reading file
      String sentenceString = fileScanner.nextLine(); //making new variable, using next line of file
      Sentence sentence = new Sentence(sentenceString); //taking new variable and making a new Sentence
      this.addSentence(sentence); // see line 17
    }
  }

  public void printSentences() { //error leads here
      for(int i = 0; i < sentences.length; i++) { //variable i is set to zero here; i is less then sentence length
        if (sentences[i] == null) { //this is me
          continue; //this is me
        }
      System.out.println(((Sentence)sentences[i]).getSentence()); //new error is focused on this line,
      //changing the sentence variable does not help
    }
  }


  public void reverseSentences() { //and now i'm supposed to reverse the array?
    for (int i = 9; i > sentences.length/2; i--){ //me again
      Sentence temp; //making a temporary empty space
      //sentences[sentences.length - i - 1];
      temp = sentences[sentences.length - i - 1]; //move first to temporary
      sentences[sentences.length - i - 1] = sentences[i]; //move end to first
      sentences[i] = temp; //move temp to end
   }
  }


}
