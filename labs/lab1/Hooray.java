public class Hooray {

    public static void hooray() {
        while(true) {            //while statement is always true so hooray is continues to be printed out
            System.out.println("Hooray!");
        }
    }

    public static void main(String[] args) {
        hooray();
    }
//this program continues to output the word hooray in essence forever.
//it does this because of the while statement which will always be true
}
