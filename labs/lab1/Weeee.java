public class Weeee {

    public static void weeee() {
        System.out.println("Weeee!");
        weeee();
    }

    public static void main(String[] args) {
        weeee();
    }
//unlike the program hooray.java this program doesn't contain a while statement and will eventually stop
//on it's own. the program is calling upon itself but computers have a finite amount
//of memory, eventually the memory fills up and the program crashes
}
