//****************************************
// Honor Code: The work I am Submitting is a result of my own thinking and efforts.
// Gabriella Amara Belt
// CMPSC 112 Fall 2016
// Lab 8
// Date: Nov 6th, 2016
//
// Purpose: checks the length (between 10 and 15 characters), that the password consists of only letters/numbers, and the presence of at least two numbers. 
//***************************************

public class Checks {

    public static boolean validPassword(String checkedPassword) {  
    
        if (checkedPassword.length() < 10 || (checkedPassword.length() > 15)) {   // checking that length is more than 10 and less than 15
            return false;                                      
        }
        
        char c;                                                 // Checks that password only contains letters and numbers
        int count = 0;

        for (int i = 0; i < checkedPassword.length() - 1; i++) {  
            c = checkedPassword.charAt(i);  
            if (!Character.isLetterOrDigit(c)) {          
                return false;
            } else if (Character.isDigit(c)) {                  //counts number of digits
                count++;
            }     
        }
        
        return (count >= 2);                                     // Checks that two numbers are present 
    }
}


	
	
